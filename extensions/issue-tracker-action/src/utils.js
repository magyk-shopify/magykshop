export async function getDescription(productId) {
    const response = await makeGraphQLQuery(
      `query Product($id: ID!) {
        product(id: $id) {
          title
          metafield(namespace: "$app:description", key: "description") {
            value
          }
          images(first: 1) {
            edges {
              node {
                url
              }
            }
          }
        }
      }`,
      { id: productId }
    );

    if (!response.data || !response.data.product) {
      throw new Error("Product data is missing.");
    }

    const product = response.data.product;
    const image = product.images.edges.length > 0 ? product.images.edges[0].node.url : null;
  
    return {
      ...product,
      image
    }
}

export async function updateDescription(productId, newDescription) {
  console.log("Updating description for product: ", productId);

  return await makeGraphQLQuery(
    `mutation UpdateProductDescription($id: ID!, $dHtml: String!) {
      productUpdate(input: {id: $id, descriptionHtml: $dHtml}) {
        product {
          id
          title
          descriptionHtml
        }
        userErrors {
          field
          message
        }
      }
    }`,
    {
      id: productId,
      dHtml: newDescription,
    }
  );
}

async function makeGraphQLQuery(query, variables) {
    const graphQLQuery = {
      query,
      variables,
    };
  
    const res = await fetch("shopify:admin/api/graphql.json", {
      method: "POST",
      body: JSON.stringify(graphQLQuery),
      headers: {
        'Content-Type': 'application/json',
      }
    });
   
    if (!res.ok) {
      console.error("Network error:", res.status, res.statusText);
    }
  
    const jsonResponse = await res.json();
    return jsonResponse;
}
