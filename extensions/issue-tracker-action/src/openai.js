import OpenAI from 'openai';

const openai = new OpenAI({
  apiKey: "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"
});

async function detailsToDescription(title = '', imageUrl, ageRange = '', gender = '', country = '', positioning = '') {
  if (!imageUrl) {
    throw new Error("imageUrl parameter is required");
  }

  // Default prompt
  let prompt = `
    Prompt:
    Describe the colour of the product in the image:
  `;

  // Extended prompt if other parameters are provided
  if (title || ageRange || gender || country || positioning) {
    prompt = `
      Prompt:
      Generate a product description for the following title and image, and use the targeted audience details to improve the description.

      Title: ${title}
      Image URL: ${imageUrl}
      Targeted Audience Details:
      - Age Range: ${ageRange}
      - Gender: ${gender}
      - Country or Location: ${country}
      - Positioning: ${positioning}

      Generate a product description that effectively communicates the features and benefits of the product to the specified audience. Avoid adding any additional commentary beyond the product description.

      Product Description:
    `;
  }

  const params = {
    model: 'gpt-4o',
    messages: [
      {
        role: 'system',
        content: 'You are a marketing copywriter tasked with creating a compelling product description for a new item. Your task is to generate a product description based on the provided title and details of the targeted audience.'
      },
      {
        role: 'user',
        content: [
          { type: 'text', text: prompt },
          {
            type: 'image_url',
            image_url: {
              url: imageUrl,
            },
          },
        ],
      },
    ],
    max_tokens: 1000,
  };

  const response = await openai.chat.completions.create(params);
  console.log('Generated Product Description', JSON.stringify({ request: params, response }));
  return response.choices[0].message.content;
}


export async function fetchDescriptionFromOpenAI(title, imageUrl, ageRange, gender, country, positioning) {
  console.log("Fetching description for title:", title);
  const description = await detailsToDescription(title, imageUrl, ageRange, gender, country, positioning);

  if (!description) {
    console.error("Error fetching description from OpenAI");
    throw new Error(`Error fetching description`);
  }
  return description;
}
